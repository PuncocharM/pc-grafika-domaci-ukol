#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <math.h>
#include <vector>
#include <GL/glut.h>
#include <GL/glext.h>
#include <fstream>
#include <iostream>
#include <gemmi/cif.hpp>

using namespace std;
namespace cif = gemmi::cif;

static const string MOL_FILENAME = "buprenorfin_hydrochlorid.cif";
static const float MOUSE_ROT_SPEED = 0.1;
static const float MOUSE_SCALE_SPEED = 0.005;
static const float WALK_SPEED = 0.2;
static const float WALK_ROT_SPEED = 0.02;
static const float BOND_DIST_TOLERANCE = 0.3;

struct Atom {
    string label;
    string symbol;
    float x, y, z;

    Atom(string &label, string &symbol, float x, float y, float z) {
        this->label = label;
        this->symbol = symbol;
        this->x = x;
        this->y = y;
        this->z = z;
    }
};

struct Molecule {
    vector<Atom> atoms;
    vector<pair<Atom*,Atom*>> bonds;
    vector<pair<Atom*,Atom*>> non_bond_interactions;
};

Molecule * molecule;

bool mouse_left_down = false;
bool mouse_right_down = false;
int mouse_last_x = 0;
int mouse_last_y = 0;

float mol_rot_x = 0;
float mol_rot_y = 0;
float mol_scale = 1;

float cam_pos_x = 0;
float cam_pos_y = 0;
float cam_pos_z = 20;
float cam_rot_y = 0;


inline double square(double a) {
    return a*a;
}

void generate_bonds(Molecule * molecule, map<string, double> atom_radii) {
    for (auto &atom1 : molecule->atoms) {
        for (auto &atom2 : molecule->atoms) {
            if (&atom1 >= &atom2) {
                continue;
            }
            double dist = sqrt(square(atom1.x - atom2.x) + square(atom1.y - atom2.y) + square(atom1.z - atom2.z));
            double mean_dist = atom_radii[atom1.symbol] + atom_radii[atom2.symbol];
            double max_dist = mean_dist + BOND_DIST_TOLERANCE;
//            double min_dist = mean_dist - BOND_DIST_TOLERANCE;

//            if ((dist >= min_dist) && (dist <= max_dist)) {
            if (dist <= max_dist) {
                molecule->bonds.emplace_back(&atom1, &atom2);
            }
        }
    }
}

void generate_non_bond_interactions(Molecule * molecule) {
    // calculate the minimum distance (in bonds) between each atom pairs

    int infty =  molecule->atoms.size()*10 + 10;

    map<Atom*, map<Atom*, int>> d;

    for (auto &atom1 : molecule->atoms) {
        for (auto &atom2 : molecule->atoms) {
            d[&atom1][&atom2] = infty;
        }
    }

    for (auto bond : molecule->bonds) {
        d[bond.first][bond.second] = 1;
        d[bond.second][bond.first] = 1;
    }

    for (auto &atom1 : molecule->atoms) {
        for (auto &atom2 : molecule->atoms) {
            for (auto &atom3 : molecule->atoms) {
                if (d[&atom1][&atom3] + d[&atom3][&atom2] < d[&atom1][&atom2]) {
                    d[&atom1][&atom2] = d[&atom1][&atom3] + d[&atom3][&atom2];
                }
            }
        }
    }

    // generate non-bond interactions (at least 4 bonds away from each other)

    for (auto &atom1 : molecule->atoms) {
        for (auto &atom2 : molecule->atoms) {
            if (&atom1 >= &atom2) {
                continue;
            }
            if (d[&atom1][&atom2] > 3) {
                molecule->non_bond_interactions.emplace_back(&atom1, &atom2);
            }
        }
    }
}


void center_molecule(Molecule *molecule) {
    float center_x = 0, center_y = 0, center_z = 0;
    for (Atom &atom : molecule->atoms) {
        center_x += atom.x;
        center_y += atom.y;
        center_z += atom.z;
    }
    center_x /= molecule->atoms.size();
    center_y /= molecule->atoms.size();
    center_z /= molecule->atoms.size();

    for (Atom &atom : molecule->atoms) {
        atom.x -= center_x;
        atom.y -= center_y;
        atom.z -= center_z;
    }
}


Molecule * read_cif(const string &cif_filename) {
    auto * molecule = new Molecule();

    cif::Document doc = cif::read_file(cif_filename);
    cif::Block & block = doc.blocks[0];

    float cell_length_a = stof(*block.find_value("_cell_length_a"));
    float cell_length_b = stof(*block.find_value("_cell_length_b"));
    float cell_length_c = stof(*block.find_value("_cell_length_c"));

    cif::Table atoms_tbl = block.find({"_atom_site_label", "_atom_site_type_symbol", "_atom_site_fract_x", "_atom_site_fract_y", "_atom_site_fract_z"});
    for (cif::Table::Row row : atoms_tbl) {
        Atom * atom = new Atom(row[0], row[1], cell_length_a*stof(row[2]), cell_length_b*stof(row[3]), cell_length_c*stof(row[4]));
        molecule->atoms.push_back(*atom);
    }

    map<string, double> atom_radii;
    cif::Table radii_tbl = block.find({"_atom_type_symbol", "_atom_type_radius_bond"});
    for (cif::Table::Row row : radii_tbl) {
        atom_radii[row[0]] = stod(row[1]);
    }

    generate_bonds(molecule, atom_radii);
    generate_non_bond_interactions(molecule);
    center_molecule(molecule);

    return molecule;
}


void InitGLLighting() {
    GLfloat mat_specular[] = {1.0, 1.0, 1.0, 1.0};
    GLfloat mat_shininess[] = {50.0};
    GLfloat light_position[] = {1.0, 1.0, 1.0, 0.0};

    glClearColor(0.0, 0.0, 0.0, 0.0);
    glShadeModel(GL_SMOOTH);

    glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_COLOR_MATERIAL);
}


void SetupCamera(float aspect_ratio) {
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45, aspect_ratio, 0.1, 100);
    glMatrixMode(GL_MODELVIEW);
}


GLvoid ReSizeGLScene(GLsizei width, GLsizei height) {
    if (height == 0) {                                        // Prevent A Divide By Zero By
        height = 1;                                        // Making Height Equal One
    }
    glViewport(0, 0, width, height);                        // Reset The Current Viewport

    float aspect = (GLfloat) width / (GLfloat) height;
    SetupCamera(aspect);
}


void display_atoms() {
    float atom_size;
    for (Atom &atom : molecule->atoms) {
        if (atom.symbol == "H") {
            glColor3f(1, 1, 1); // white
            atom_size = 0.2;
        } else if (atom.symbol == "N") {
            glColor3f(0, 0, 1); // blue
            atom_size = 0.5;
        } else if (atom.symbol == "C") {
            glColor3f(0.5, 0.5, 0.5); // green
            atom_size = 0.5;
        } else if (atom.symbol == "O") {
            glColor3f(1, 0, 0); // red
            atom_size = 0.5;
        } else if (atom.symbol == "S") {
            glColor3f(1, 1, 0); // yellow
            atom_size = 0.5;
        } else if (atom.symbol == "P") {
            glColor3f(1, 0.5, 0); // orange
            atom_size = 0.5;
        } else {
            glColor3f(0.5, 0, 0); // dark red
            atom_size = 0.5;
        }

        glPushMatrix();
        glTranslatef(atom.x, atom.y, atom.z);
        glutSolidSphere(atom_size, 100, 100);
        glPopMatrix();
    }
}

void display_bonds() {
    glColor3f(1, 1, 1); // white
    glLineWidth(5.0f * mol_scale); // thick
    glLineStipple(1, 0xFFFF); // solid
    glBegin(GL_LINES);
    for (auto &bond : molecule->bonds) {
        glVertex3f(bond.first->x, bond.first->y, bond.first->z);
        glVertex3f(bond.second->x, bond.second->y, bond.second->z);
    }
    glEnd();
}


void display_non_bond_interactions() {
    glColor3f(0.8, 0.8, 0.8);  // light grey
    glLineWidth(0.2f * mol_scale);  // thin
    glLineStipple(5, 0xAAAA);  // dashed
    glEnable(GL_LINE_STIPPLE);
    glBegin(GL_LINES);
    for (auto &non_bond : molecule->non_bond_interactions) {
        glVertex3f(non_bond.first->x, non_bond.first->y, non_bond.first->z);
        glVertex3f(non_bond.second->x, non_bond.second->y, non_bond.second->z);
    }
    glEnd();
}

inline float rad_to_deg(float rad) {
    return rad * 180 / 3.1415f;
}

void display() {
    glDrawBuffer(GL_BACK);
    glClearColor(0.0, 0.0, 0.0, 1.0); /* black */
    glClear(GL_COLOR_BUFFER_BIT);
    glClear(GL_DEPTH_BUFFER_BIT);

    glPushMatrix();

    glRotatef(-rad_to_deg(cam_rot_y), 0, 1, 0);
    glTranslatef(-cam_pos_x, -cam_pos_y, -cam_pos_z);
    glRotatef(mol_rot_x, 1, 0, 0);
    glRotatef(mol_rot_y, 0, 1, 0);
    glScalef(mol_scale, mol_scale, mol_scale);

    display_atoms();
    display_bonds();
    display_non_bond_interactions();

    glPopMatrix();
    glutSwapBuffers();
}

void keyboard(unsigned char key, int x, int y) {
  //
}


void special_input(int key, int x, int y) {
    switch (key) {
        case 101: // UP
            cam_pos_x -= sin(cam_rot_y) * WALK_SPEED;
            cam_pos_z -= cos(cam_rot_y) * WALK_SPEED;
            break;
        case 103: // DOWN
            cam_pos_x += sin(cam_rot_y) * WALK_SPEED;
            cam_pos_z += cos(cam_rot_y) * WALK_SPEED;
            break;
        case 100: // LEFT
            cam_rot_y += WALK_ROT_SPEED;
            break;
        case 102: // RIGHT
            cam_rot_y -= WALK_ROT_SPEED;
            break;
        case 104: // PG UP
            cam_pos_y += WALK_SPEED;
            break;
        case 105: // PG DOWN
            cam_pos_y -= WALK_SPEED;
            break;
        default:
            break;
    }
    display();
}

void mouse(int button, int state, int x, int y) {
    if (button == GLUT_LEFT_BUTTON) {
        mouse_left_down = (state == GLUT_DOWN);
    }
    if (button == GLUT_RIGHT_BUTTON) {
        mouse_right_down = (state == GLUT_DOWN);
    }
    mouse_last_x = x;
    mouse_last_y = y;
}

void motion(int x, int y) {
    int dx = x - mouse_last_x;
    int dy = y - mouse_last_y;
    mouse_last_x = x;
    mouse_last_y = y;

    if (mouse_left_down) {
        mol_rot_y += dx * MOUSE_ROT_SPEED;
        mol_rot_x += dy * MOUSE_ROT_SPEED;
        display();
    } else if (mouse_right_down) {
//        double d = sqrt(x*x + y*y);
        mol_scale *= exp(MOUSE_SCALE_SPEED*dx);
        display();
    }
}

int main(int argc, char **argv) {
    molecule = read_cif(MOL_FILENAME);
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_DEPTH | GLUT_RGB);
    glutCreateWindow("PC Grafika - Domaci ukol");
    InitGLLighting();
    SetupCamera(1);
    glEnable(GL_DEPTH_TEST);
    glutDisplayFunc(display);
    glutKeyboardFunc(keyboard);
    glutSpecialFunc(special_input);
    glutReshapeFunc(ReSizeGLScene);
    glutMouseFunc(mouse);
    glutMotionFunc(motion);
    glutMainLoop();
    return 0;
}
