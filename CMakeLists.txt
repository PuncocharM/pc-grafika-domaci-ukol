cmake_minimum_required(VERSION 3.12)
project(Homework)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_EXE_LINKER_FLAGS "-static-libgcc -static-libstdc++ -static")

set(GLUT_glut_LIBRARY "f:/data/skola/VSCHT/07-semestr-2018-19-Z/pocitacova-grafika/freeglut/lib/freeglut.lib")
set(GLUT_ROOT_PATH "f:/data/skola/VSCHT/07-semestr-2018-19-Z/pocitacova-grafika/freeglut")
set(gemmi_INCLUDE "f:/data/skola/VSCHT/07-semestr-2018-19-Z/pocitacova-grafika/gemmi/include")
set(gemmi_3p_INCLUDE "f:/data/skola/VSCHT/07-semestr-2018-19-Z/pocitacova-grafika/gemmi/third_party")

find_package (OpenGL REQUIRED)
find_package (GLUT REQUIRED)

include_directories(${OPENGL_INCLUDE_DIR} ${GLUT_INCLUDE_DIR} ${gemmi_INCLUDE} ${gemmi_3p_INCLUDE})

if(NOT OPENGL_FOUND)
    message(ERROR "OPENGL not found!")
endif(NOT OPENGL_FOUND)

add_executable(Homework homework.cpp)

target_link_libraries(Homework ${OPENGL_LIBRARIES} ${GLUT_LIBRARIES})
