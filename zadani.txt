1) Data načíst ze souboru:
   buprenorfin_hydrochlorid.cif
2) Atomy zobrazit (různé barvy podle typu) jako
   Kuličky
3) Vazby zobrazit jako:
   Čáry
4) S modelem interagovat
   Procházení (změna směru, nahoru dolu, dopředu dozadu)
5) Jako doplňkovou informaci :
   Zobrazit v molekule kromě vazeb i nevazebné interakce (odlišit od vazeb)