# PC Grafika Domaci Ukol

Ovládání:
* Procházení šipkami 
* Otáčení molekuly levým tlačítkem myši
* Velikost molekuly pravým tlačítkem myši


Atomy jsou zobrazeny standartními barvami. Vazby jsou zobrazeny plnou tlustou čarou. Nevazebné interakce (přes 3 a více vazeb) jsou zobrazeny teknkou přerušovanou čarou.

Na parsování cif souboru používám https://gemmi.readthedocs.io/en/latest/cif-parser.html